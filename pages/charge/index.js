const util = require('../../utils/util.js')
const { API_PREFIX } = require('../../utils/config.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userId: '',       // 用户id
    cashbookId: '',   // 记账本id
    avatarUrl: '',    // 用户头像
    
    type: 'pay',      // 收入 or 支出
    payClassifies: [   // 支出分类
      { icon: 'eat', name: '吃喝' },
      { icon: 'traffic', name: '交通' },
      { icon: 'clothes', name: '服饰' },
      { icon: 'red_envelopes', name: '红包' },
      { icon: 'vegetables', name: '买菜' },
      { icon: 'book', name: '学习' },
      { icon: 'medicalCare', name: '医疗' },
      { icon: 'fixedDeposit', name: '固定存款' },
      { icon: 'floatingDeposit', name: '流动存款' },
      { icon: 'other', name: '其它' },
    ],
    incomeClassifies: [ // 收入分类
      { icon: 'salary', name: '工资' },
      { icon: 'red_envelopes', name: '红包' },
      { icon: 'investment', name: '投资' },
      { icon: 'bonus', name: '奖金' },
      { icon: 'other', name: '其它' },
    ],
    activeClassify: '', // 默认分类

    amt: '',        // 表单：金额
    remark: '',     // 表单：备注
    date: '',       // 表单：日期
    picUrl: '',     // 备注拍照
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const { userId, cashbookId, avatarUrl } = options
    this.setData({
      userId,
      cashbookId,
      avatarUrl,
      activeClassify: this.data.payClassifies[0].name,
      date: util.formatDate(new Date()),
    });
  },

  /**
   * 修改类型：支出 or 收入
   */
  handleChangeType: function (e) {
    const type = e.currentTarget.dataset.type
    const { payClassifies, incomeClassifies } = this.data
    this.setData({
      type,
      activeClassify: type === 'pay' ? payClassifies[0].name : incomeClassifies[0].name,
    })
  },

  /**
   * 选择分类
   */
  handleChangeClassify: function (e) {
    const classify = e.currentTarget.dataset.classify
    this.setData({ activeClassify: classify })
  },

  /**
   * 日期值变化触发事件
   */
  handleChangeDate: function (e) {
    this.setData({
      date: e.detail.value
    })
  },

  /**
   * 拍照
   */
  handlePhotograph: function () {
    const that = this
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths)
        that.setData({
          picUrl: tempFilePaths
        })
      }
    })
  },

  /**
   * 提交表单
   */
  handleSubmitForm: function (e) {
    const { amt, date, remark } = e.detail.value
    const { cashbookId, userId, avatarUrl, type, activeClassify, picUrl } = this.data

    if (!amt) {
      wx.showToast({
        title: '金额不能为空',
      })
      return
    }

    wx.request({
      url: `${API_PREFIX}/detail`,
      method: 'POST',
      data: {
        cashbookId,
        userId,
        userAvatar: avatarUrl,
        type: activeClassify,
        amt,
        remark,
        picUrl,
        amtType: type === 'pay' ? -1 : 1,
        creatDate: date,
      },
      success: function (res) {
        wx.navigateTo({
          url: '../home/index',
        })
      },
      fail: function (err) {
        console.error(err)
      }
    })
  },
})