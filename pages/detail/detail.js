// pages/detail/detail.js
const { API_PREFIX } = require('../../utils/config.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    detailList:[],
    id:'',
    createDate:'',
    amt:'',
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    this.setData({
      id: options.id,
      createDate: options.createDate
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this
    wx.request({
      url: `${API_PREFIX}/detail/${that.data.id}`,
      method: 'GET',
      success: function (res) {
        
        const detailList = res.data.data
        console.log(detailList)
        that.setData({
          detailList
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handleEnlargePic:function(){
    var that = this
    wx.previewImage({
      
      current: `${that.data.detailList[0].pic_url}`, // 当前显示图片的http链接
      urls: [`${that.data.detailList[0].pic_url}`] // 需要预览的图片http链接列表
    })
  },
  detailRemark: function (event){
    // event.detail 为当前输入的值
    this.setData({
      remark: event.detail.value
    })
  },
  detailAmt: function (event){
    var that = this;
    this.setData({
      amt: event.detail.value
    })
    console.log(that.data.amt)
  },
  detailType: function (event){
    this.setData({
      type: event.detail.value
    })
  },

  updateDetail:function(){
    var that = this
    wx.request({
      url: `${API_PREFIX}/detail/${that.data.id}`,
      method: 'PUT',
      data: {
        type: that.data.type,
        amt: that.data.amt,
        remark: that.data.remark,
        picUrl: that.data.picUrl,
      },
      success: function (res) {
        wx.navigateTo({
          url: '../home/index'
        })
        console.log(that.data.remark)
      },
      fail: function (err) {
        console.error(err)
      }
    })
  }

})